from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from logica import calcular_total

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///orders.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Orden(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    restaurante = db.Column(db.String(50), nullable=False)
    combo = db.Column(db.String(50), nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    direccion = db.Column(db.String(200), nullable=False)
    telefono = db.Column(db.String(20), nullable=False)
    metodo_pago = db.Column(db.String(50), nullable=False)
    numero_tarjeta = db.Column(db.String(20), nullable=True)
    total = db.Column(db.Float, nullable=False, default=0.0)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/confirmar_orden', methods=['POST'])
def confirmar_orden():
    data = request.get_json()
    if not data:
        return jsonify({'message': 'No data provided'}), 400

    restaurante = data.get('restaurante')
    combo = data.get('combo')
    nombre = data.get('nombre')
    direccion = data.get('direccion')
    telefono = data.get('telefono')
    metodo_pago = data.get('metodo_pago')
    numero_tarjeta = data.get('numero_tarjeta')

    total = calcular_total(combo)

    nueva_orden = Orden(
        restaurante=restaurante,
        combo=combo,
        nombre=nombre,
        direccion=direccion,
        telefono=telefono,
        metodo_pago=metodo_pago,
        numero_tarjeta=numero_tarjeta,
        total=total
    )

    db.session.add(nueva_orden)
    db.session.commit()

    return jsonify({'message': 'Orden confirmada', 'orden': {
        'id': nueva_orden.id,
        'restaurante': restaurante,
        'combo': combo,
        'nombre': nombre,
        'direccion': direccion,
        'telefono': telefono,
        'metodo_pago': metodo_pago,
        'numero_tarjeta': numero_tarjeta,
        'total': total
    }})

@app.route('/modificar_orden/<int:orden_id>', methods=['PUT'])
def modificar_orden(orden_id):
    data = request.get_json()
    orden = Orden.query.get_or_404(orden_id)

    if 'restaurante' in data:
        orden.restaurante = data['restaurante']
    if 'combo' in data:
        orden.combo = data['combo']
    if 'nombre' in data:
        orden.nombre = data['nombre']
    if 'direccion' in data:
        orden.direccion = data['direccion']
    if 'telefono' in data:
        orden.telefono = data['telefono']
    if 'metodo_pago' in data:
        orden.metodo_pago = data['metodo_pago']
    if 'numero_tarjeta' in data:
        orden.numero_tarjeta = data['numero_tarjeta']

    if 'combo' in data:
        orden.total = calcular_total(data['combo'])

    db.session.commit()
    return jsonify({'message': 'Orden modificada', 'orden': {
        'id': orden.id,
        'restaurante': orden.restaurante,
        'combo': orden.combo,
        'nombre': orden.nombre,
        'direccion': orden.direccion,
        'telefono': orden.telefono,
        'metodo_pago': orden.metodo_pago,
        'numero_tarjeta': orden.numero_tarjeta,
        'total': orden.total
    }})

@app.route('/ordenes', methods=['GET'])
def listar_ordenes():
    ordenes = Orden.query.all()
    return jsonify([{
        'id': orden.id,
        'restaurante': orden.restaurante,
        'combo': orden.combo,
        'nombre': orden.nombre,
        'direccion': orden.direccion,
        'telefono': orden.telefono,
        'metodo_pago': orden.metodo_pago,
        'numero_tarjeta': orden.numero_tarjeta,
        'total': orden.total
    } for orden in ordenes])

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True)
