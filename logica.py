def calcular_total(combo):
    precios = {
        'combo_1': 5.99,
        'combo_2': 7.99,
        'combo_3': 9.99
    }
    return precios.get(combo, 0)
