# Proyecto de Bases de Datos I: Aplicación de Gestión de Órdenes de Comida

Este proyecto es una aplicación web simple para gestionar órdenes de comida, desarrollado como parte del curso de Bases de Datos I. El objetivo principal de esta aplicación es la inserción y modificación de datos en una base de datos. La aplicación fue desarrollada con la ayuda de ChatGPT de OpenAI.

## Descripción del Proyecto

La aplicación permite a los usuarios:
- Insertar nuevas órdenes de comida.
- Modificar órdenes de comida existentes.
- Consultar todas las órdenes de comida almacenadas en la base de datos.

## Tecnologías Utilizadas

- **Backend**: Flask (Python)
- **Base de Datos**: SQLite
- **Frontend**: HTML, Bootstrap, JavaScript (Fetch API)
- **ORM**: SQLAlchemy
- **ChatGPT**

## Requisitos

Asegúrate de tener instalado lo siguiente antes de ejecutar la aplicación:

- Python 3.x
- Flask
- Flask-SQLAlchemy


