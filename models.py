from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Orden(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    restaurante = db.Column(db.String(50), nullable=False)
    combo = db.Column(db.String(50), nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    direccion = db.Column(db.String(200), nullable=False)
    telefono = db.Column(db.String(20), nullable=False)
    metodo_pago = db.Column(db.String(50), nullable=False)
    numero_tarjeta = db.Column(db.String(20), nullable=True)
    total = db.Column(db.Float, nullable=False, default=0.0)
